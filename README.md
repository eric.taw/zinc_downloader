# Asynchronous Downloading and Multithreaded Unzipping for ZINC 

## Steps
- Obtain a text file with a bunch of URIs pointing to various files from ZINC using the [Tranches Browser](http://zinc15.docking.org/tranches/home/) ([wiki link here](http://wiki.docking.org/index.php/Tranche_Browser)) and place it in same folder as `process.py`. The included `ZINC-downloader-3D-sdf.gz.uri` points to ~10m molecules with 3D structures at near-neutral pH
- Edit `process.py` to point to the right URI file
- (optional) Set up a new virtual environment `python -m venv [venv-name]`
- Run `pip install -r requirements.txt`
- Run `python process.py`. Zipped files will appear in `./raw`, and unzipped files will appear in `./unzipped`