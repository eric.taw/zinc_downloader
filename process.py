from multiprocessing import Pool, cpu_count
import gzip
import glob
import os
import aiohttp
import aiofiles
import asyncio

class PostProcess:

    @staticmethod
    def unpack(filename: str) -> None:
        """
        Extracts the file at filename to ./unzipped
        
        Arguments:
            filename {str} -- filename pointing to zipped file
        """
        with gzip.open(filename) as f:
            contents = f.read()
            filename = filename.replace('raw', 'unzipped')[:-3]
            with open(filename, 'wb') as uncompressed:
                uncompressed.write(contents)

    @staticmethod
    def parallel_unzip():
        """
        Obtains all zipped files and unzips them in parallel
        """
        print('Unpacking all gzipped files')
        filenames = glob.glob('./raw/*.gz')
        with Pool(processes=cpu_count()) as pool:
            pool.map(PostProcess.unpack, filenames)


class Download:

    @staticmethod
    async def fetch(session: aiohttp.ClientSession, url: str):
        """
        Obtains the raw file from ZINC
        
        Arguments:
            session {aiohttp.ClientSession}
            url {str} -- URL pointing to the compressed file
        """
        async with session.get(url) as response:
            if response.status == 200:
                filename = url.split('/')[-1]
                async with aiofiles.open('./raw/' + filename, mode='wb') as f:
                    return await f.write(await response.content.read())
            else:
                print('URL %s had a response status code of %i' % (url, response.status))

    @staticmethod
    async def main(url: str, sem: asyncio.Semaphore) -> None:
        """
        Obtains a single file from one URL
        
        Arguments:
            url {str} -- URL pointing to the compressed file
            sem {asyncio.Semaphore} -- Semaphore object prevents too many concurrent requests
        """
        print('Downloading from ' + url)
        async with sem:
            async with aiohttp.ClientSession() as session:
                await Download.fetch(session, url)

    @staticmethod
    def download_all(uri_path: str) -> None:
        """
        From a file containing all URLs, downloads everything asynchronously into ./raw
        
        Arguments:
            uri_path {str} -- file path to URIs file
        """
        if not os.path.exists('./raw'):
            os.mkdir('./raw')
        if not os.path.exists('./unzipped'):
            os.mkdir('./unzipped')

        if not os.listdir('./raw') and not os.listdir('./unzipped'):
            with open(uri_path, 'r') as uri_file:
                urls = [line.rstrip('\n') for line in uri_file]
                loop = asyncio.get_event_loop()
                sem = asyncio.Semaphore(10)
                tasks = (Download.main(url, sem) for url in urls)
                loop.run_until_complete(asyncio.gather(*tasks))


if __name__ == '__main__':
    Download.download_all('ZINC-downloader-3D-sdf.gz.uri')
    PostProcess.parallel_unzip()
    